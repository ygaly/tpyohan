﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Hotel
    {
        private int id;
        private string nom;
        private string adresse;
        private string ville;

        public Hotel(int id, string nom, string adresse, string ville)
        {
            this.id = id;
            this.nom = nom;
            this.adresse = adresse;
            this.ville = ville;
        }

        public int Id { get => id; set => id = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Adresse { get => adresse; set => adresse = value; }
        public string Ville { get => ville; set => ville = value; }
    }
}
