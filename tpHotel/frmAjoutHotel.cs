﻿using System;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutHotel : Form
    {
        private string nom;
        private string adresse;
        private string ville;

        public frmAjoutHotel()
        {
            InitializeComponent();
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.raz();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {

            nom = txtNom.Text;
            adresse = txtAdresse.Text;
            ville = txtVille.Text;

            Persistance.ajouteHotel(txtNom.Text, txtAdresse.Text, txtVille.Text);

            MessageBox.Show(" L'hotel " + nom + adresse + ville + " a été crée ");

           
        }

        private void raz()
        {
            this.txtNom.Text = "";
            this.txtAdresse.Text = "";
            this.txtVille.Text = "";
            this.txtNom.Focus();
        }
    }
}
