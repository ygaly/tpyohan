﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Chambre
    {
        int numetage;
        string description;
        int idhotel;
        int num;

        public int Numetage { get => numetage; set => numetage = value; }
        public string Description { get => description; set => description = value; }
        public int Idhotel { get => idhotel; set => idhotel = value; }
        public int Num { get => num; set => num = value; }

        public Chambre(int numetage, int idhotel, int num, string description)
        {
            this.Numetage = numetage;
            this.Description = description;
            this.Idhotel = idhotel;
            this.Num = num;
        }

        
    }
}
