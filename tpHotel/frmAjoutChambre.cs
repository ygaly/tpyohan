﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutChambre : Form
    {
        private string etage;
        private string description;
        private string hotel;

        public frmAjoutChambre()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            foreach (Hotel item in Persistance.getLesHotels())
            {
                if (item.Nom == comboBox1.Text)
                {
                    Persistance.ajouteChambre((int)numericUpDown1.Value, textBox1.Text, item.Id);
                }
            }
            

            MessageBox.Show(" La chambre à l'étage " + numericUpDown1.Value + "de l'hotel" + textBox1.Text + " a été crée, " + "Description : " + comboBox1.Text);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void frmAjoutChambre_Load(object sender, EventArgs e)
        {
            foreach (Hotel Item in Persistance.getLesHotels())
            {
                
                comboBox1.Items.Add(Item.Nom);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            numericUpDown1.ResetText();
            textBox1.ResetText();
            comboBox1.ResetText();
        }
    }
}
